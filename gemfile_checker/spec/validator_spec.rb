require 'rspec'
require_relative '../model/validator'

describe Validator do

  subject { @validator = Validator.new }

  it { should respond_to (:process) }

  it 'should raise "Error: Gemfile vacio" if gemfile is empty' do
    gemfile_string = ''
    begin
      result = subject.process gemfile_string
      expect(false).to eq true
    rescue StandardError => e
      expect(e.message).to eq('Error: Gemfile vacio')
    end
  end

  it 'should raise "Error: Gemfile sin source" if gemfile does not begin with source line' do
    gemfile_content = File.read "#{__dir__}/samples/Gemfile.invalid.source"
    begin
      result = subject.process gemfile_content
      expect(false).to eq true
    rescue StandardError => e
      expect(e.message).to eq('Error: Gemfile sin source')
    end
  end

  it 'should raise "Error: Gemfile sin version de ruby" if gemfile does not have a ruby version' do
    gemfile_content = File.read "#{__dir__}/samples/Gemfile.invalid.ruby"
    begin
      result = subject.process gemfile_content
      expect(false).to eq true
    rescue StandardError => e
      expect(e.message).to eq('Error: Gemfile sin version de ruby')
    end
  end

  it 'should raise "Error: Gemfile con gemas desordenadas" if gems are not ordered alphabetically' do
    gemfile_content = File.read "#{__dir__}/samples/Gemfile.invalid.gems"
    begin
      result = subject.process gemfile_content
      expect(false).to eq true
    rescue StandardError => e
      expect(e.message).to eq('Error: Gemfile con gemas desordenadas')
    end
  end

  it 'should raise "Error: Gemfile con línea de gema corrupta" if has gem line that does not start with "gem"' do
    gemfile_content = File.read "#{__dir__}/samples/Gemfile.invalid.corrupted.gem"
    begin
      result = subject.process gemfile_content
      expect(false).to eq true
    rescue StandardError => e
      expect(e.message).to eq('Error: Gemfile con línea de gema corrupta')
    end
  end

  it 'should return "Gemfile correcto" when valid Gemfile' do
    gemfile_content = File.read  "#{__dir__}/samples/Gemfile.valid"
    validation_result = Validator.new.process gemfile_content
    expect(validation_result).to eq "Gemfile correcto"
  end
end
