#!/bin/bash

set -e

function check() {
    ruby app.rb "$2" | grep "$3" >/dev/null && echo "$1:ok - $3" || echo "$1:error - $3"
}

echo 'Running acceptance tests for gchecker'
check '01' 'spec/samples/Gemfile.valid' 'Gemfile correcto'
check '02' 'spec/samples/Gemfile.invalid.empty' 'Error: Gemfile vacio'
check '03' 'spec/samples/Gemfile.invalid.source' 'Error: Gemfile sin source'
check '04' 'spec/samples/Gemfile.invalid.ruby' 'Error: Gemfile sin version de ruby'
check '05' 'spec/samples/Gemfile.invalid.gems' 'Error: Gemfile con gemas desordenadas'
check '06' 'spec/samples/Gemfile.invalid.corrupted.gem' 'Error: Gemfile con línea de gema corrupta'
