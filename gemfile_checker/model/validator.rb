require 'byebug'
class Validator
  def process(gemfile_content)
    @has_source = false
    @has_ruby_version = false
    @gems_names = []

    check_gemfile_empty gemfile_content

    gemfile_content.each_line do |line|
      next if line.strip.empty?
      next if check_gemfile_line_source line
      next if check_gemfile_line_ruby_version line
      check_gemfile_line_gem line
    end

    NO_ERROR
  end

  private

  # Error messages
  NO_ERROR = 'Gemfile correcto'
  ERROR_GEMFILE_EMPTY = 'Error: Gemfile vacio'
  ERROR_GEMFILE_MISSING_SOURCE = 'Error: Gemfile sin source'
  ERROR_GEMFILE_MISSING_RUBY_VERSION = 'Error: Gemfile sin version de ruby'
  ERROR_GEMFILE_MESSY_GEMS = 'Error: Gemfile con gemas desordenadas'
  ERROR_GEMFILE_GEM_LINE_CORRUPTED = 'Error: Gemfile con línea de gema corrupta'

  def check_gemfile_empty(gemfile_content)
    raise ERROR_GEMFILE_EMPTY unless not gemfile_content.empty?
  end

  def check_gemfile_line_source(line)
    return false if @has_source
    raise ERROR_GEMFILE_MISSING_SOURCE unless line.strip.index('source') == 0
    @has_source = true
    @has_source
  end

  def check_gemfile_line_ruby_version(line)
    return false if @has_ruby_version
    raise ERROR_GEMFILE_MISSING_RUBY_VERSION unless line.strip.index('ruby') == 0
    @has_ruby_version = true
    @has_ruby_version
  end

  def check_gemfile_line_gem(line)
    raise ERROR_GEMFILE_GEM_LINE_CORRUPTED unless line.index("gem") == 0
    gem_name = get_gem_name line
    return @gems_names.push gem_name if @gems_names.empty?
    raise ERROR_GEMFILE_MESSY_GEMS unless @gems_names[-1] < gem_name
    @gems_names.push gem_name
  end

  def get_gem_name(line)
    # source: https://stackoverflow.com/questions/9661478/how-to-return-the-substring-of-a-string-between-two-strings-in-ruby
    first_marker = "'"
    second_marker = "'"
    line[/#{first_marker}(.*?)#{second_marker}/m, 1]
  end
end
